﻿#include <iostream>

class Example
{
private:
	int a;

public:
	int GetA()
	{
		return a;
	}

	void SetA()
	{
		a = 5;
	}
};

class Vector
{
private:
	int x;
	int y;
	int z;

public:
	int GetB()
	{
		int var(int x, int y, int z);
		return sqrt(x * x + y * y + z * z);
	}

	void SetB()
	{
		x = 1;
		y = 1;
		z = 1;
	}
};

int main()
{
	Example q;
	q.SetA();
	std::cout << q.GetA() << std::endl;

	Vector w;
	w.SetB();
	std::cout << w.GetB() << std::endl;
	
};